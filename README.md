Boot2Qt BSP layer for Qualcomm boards
=====================================

This layer provides additional configuration and recipe appends to support
[Boot2Qt](https://doc.qt.io/Boot2Qt) on Qualcomm Boards.

How to use this layer
---------------------

Using this layer requires Google's repo tool to be installed, you can install
it from package manager (apt install repo) or download with:

    curl https://storage.googleapis.com/git-repo-downloads/repo -o repo
    chmod a+x repo


After installing the repo tool, run following commands to initialize the build environment.

    repo init -u git://code.qt.io/yocto/boot2qt-manifest -m qcom/dev.xml
    repo sync

    export MACHINE=qcom-armv8a
    . ./setup-environment.sh
