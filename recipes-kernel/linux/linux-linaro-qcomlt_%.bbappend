FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://profiling.cfg \
"

KERNEL_CONFIG_FRAGMENTS:append = " ${UNPACKDIR}/profiling.cfg"
