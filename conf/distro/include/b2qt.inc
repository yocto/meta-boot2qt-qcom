DEPLOY_CONF_NAME:qcom-armv8a = "Qualcomm Robotics RB5 Development Kit"

QBSP_IMAGE_CONTENT += "\
    ${IMAGE_LINK_NAME}.ext4.gz \
    ${IMAGE_LINK_NAME}.info \
    ${BOOT_IMAGES} \
"

BOOT_IMAGES ?= ""
BOOT_IMAGES:qcom-armv8a = "\
    boot-qcom-armv8a.img \
    boot-apq8016-sbc-qcom-armv8a.img \
    boot-apq8096-db820c-qcom-armv8a.img \
    boot-qcs404-evb-4000-qcom-armv8a.img \
    boot-qrb2210-rb1-qcom-armv8a.img \
    boot-qrb4210-rb2-qcom-armv8a.img \
    boot-qrb5165-rb5-qcom-armv8a.img \
    boot-sd-qcom-armv8a.img \
    boot-sd-apq8016-sbc-qcom-armv8a.img \
    boot-sdm845-db845c-qcom-armv8a.img \
    boot-sm8450-hdk-qcom-armv8a.img \
"

# ERROR: Nothing RPROVIDES 'linux-firmware-qcom-qrb4210-compute'
# ERROR: Nothing RPROVIDES 'linux-firmware-qcom-qcm2290-audio'
MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:remove = "packagegroup-firmware-rb1 packagegroup-firmware-rb2"

PREFERRED_PROVIDER_virtual/kernel = "linux-linaro-qcomlt"
